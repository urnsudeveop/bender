{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit AppEnbedder;

{$warn 5023 off : no warning about unused units}
interface

uses
  Embed, uSearch, xuArrays, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('Embed', @Embed.Register);
end;

initialization
  RegisterPackage('AppEnbedder', @Register);
end.
